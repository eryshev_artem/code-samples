var objectAssign = require('object-assign');

var FETCH_REVIEWS = require('../actions/reviews').FETCH_REVIEWS;
var FETCH_REVIEWS_SUCCESS = require('../actions/reviews').FETCH_REVIEWS_SUCCESS;
var FETCH_REVIEWS_FAILURE = require('../actions/reviews').FETCH_REVIEWS_FAILURE;

var CREATE_REVIEW = require('../actions/reviews').CREATE_REVIEW;
var CREATE_REVIEW_SUCCESS = require('../actions/reviews').CREATE_REVIEW_SUCCESS;
var CREATE_REVIEW_FAILURE = require('../actions/reviews').CREATE_REVIEW_FAILURE;

var INITIAL_STATE = {
  reviewsList: {
    reviews: [], error: null, loading: false
  },
  review: {
    status: null,
    error: null,
    loading: false
  }
};

function reducerReviews (state = INITIAL_STATE, action) {
  var error;

  switch(action.type) {
    case FETCH_REVIEWS: // start fetching reviews and set loading = true
      return objectAssign({}, state, {
        reviewsList: {
          reviews: [],
          error: null,
          loading: true
        }
      });
    case FETCH_REVIEWS_SUCCESS: // return list of reviews and make loading = false
      return objectAssign({}, state, {
        reviewsList: {
          reviews: action.payload,
          error: null,
          loading: false
        }
      });
    case FETCH_REVIEWS_FAILURE: // return error and make loading = false
      error = action.payload.data || {message: action.payload.message};
      return objectAssign({}, state, {
        reviewsList: {
          reviews: [],
          error: error,
          loading: false
        }
      });
    case CREATE_REVIEW: // creating review and set loading = true
      return objectAssign({}, state, {
        review: {
          status: null,
          error: null,
          loading: true
        }
      });
    case CREATE_REVIEW_SUCCESS: // if the review is successful created
      return objectAssign({}, state, {
        review: {
          status: action.success,
          error: null,
          loading: false
        }
      });
    case CREATE_REVIEW_FAILURE: // if the review is fail created
      error = action.payload.message || {message: action.payload};
      return objectAssign({}, state, {
        review: {
          status: null,
          error: error,
          loading: false
        }
      });
    default:
      return state;
  }
}

module.exports = reducerReviews;
