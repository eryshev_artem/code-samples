var React = require('react');
var connect = require('react-redux').connect;
var fetchReviews = require('../actions/reviews').fetchReviews;
var fetchReviewsSuccess = require('../actions/reviews').fetchReviewsSuccess;
var fetchReviewsFailure = require('../actions/reviews').fetchReviewsFailure;
var ReviewsList = require('../components/ReviewsList');

function mapStateToProps (state) {
  return {
    reviews: state.reviews.reviewsList.reviews,
    error:  state.reviews.reviewsList.error,
    loading: state.reviews.reviewsList.loading
  };
}

function mapDispatchToProps (dispatch) {

  // Returned objects that will contain 'fetchReviews' property. Method for send action to reducer

  return {
    fetchReviews: function (id) {
      dispatch(fetchReviews(id)).then(function (response) {
        var data = response.payload.data ? response.payload.data : {data: 'Network error'};
        if (!response.error) {
          dispatch(fetchReviewsSuccess(data))
        } else {
          dispatch(fetchReviewsFailure(data));
        }
      });
    }
  };
}

var ReviewsListContainer = connect(mapStateToProps, mapDispatchToProps)(ReviewsList);

module.exports = ReviewsListContainer;
