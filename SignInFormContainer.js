var React = require('react');
var connect = require('react-redux').connect;
var signInUser = require('../actions/users').signInUser;
var signInUserSuccess = require('../actions/users').signInUserSuccess;
var signInUserFailure = require('../actions/users').signInUserFailure;
var SignInForm = require('../components/SignInForm');
var authentication = require('../utils/authentication');
var validation = require('../utils/validation').validateUsernameAndPassword;

function validateAndSignInUser (dispatch, event) {
  event.preventDefault();
  var username = event.target.inputUsernameLogin.value.replace(/\s+/g, ''),
      password = event.target.inputPasswordLogin.value,
      dataToServer = {
        username: username,
        password: password
      };

  // Client side validation
  var resultOfValidation = validation(username, password, event.target, 5, 12);

  if (resultOfValidation) {
  // Creating action objects and send them to dispatcher taking place in the 'login' function
    authentication.login(dataToServer, dispatch, signInUser, signInUserSuccess, signInUserFailure, event.target);
  }
}

function mapStateToProps (state, ownProps) {
  return {
    user: state.user
  };
}

function mapDispatchToProps (dispatch) {
  return {
    signInUser: validateAndSignInUser.bind(null, dispatch)
  };
}

var SignInFormContainer = connect(mapStateToProps, mapDispatchToProps)(SignInForm);

module.exports.SignInFormContainer = SignInFormContainer;
